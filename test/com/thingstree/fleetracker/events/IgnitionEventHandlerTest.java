package com.thingstree.fleetracker.events;

import com.thingstree.fleetracker.events.IgnitionEventHandler;
import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import com.thingstree.fleetracker.BaseTest;
import com.thingstree.fleetracker.model.Event;
import com.thingstree.fleetracker.model.Position;

public class IgnitionEventHandlerTest extends BaseTest {
    
    @Test
    public void testIgnitionEventHandler() throws Exception {
        
        IgnitionEventHandler ignitionEventHandler = new IgnitionEventHandler();
        
        Position position = new Position();
        position.set(Position.KEY_IGNITION, true);
        position.setValid(true);
        Map<Event, Position> events = ignitionEventHandler.analyzePosition(position);
        assertEquals(events, null);
    }

}
