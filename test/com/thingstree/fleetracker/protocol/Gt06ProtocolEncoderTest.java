package com.thingstree.fleetracker.protocol;

import com.thingstree.fleetracker.protocol.Gt06ProtocolEncoder;
import org.junit.Test;
import com.thingstree.fleetracker.ProtocolTest;
import com.thingstree.fleetracker.model.Command;

public class Gt06ProtocolEncoderTest extends ProtocolTest {

    @Test
    public void testEncode() throws Exception {

        Gt06ProtocolEncoder encoder = new Gt06ProtocolEncoder();
        
        Command command = new Command();
        command.setDeviceId(1);
        command.setType(Command.TYPE_ENGINE_STOP);

        verifyCommand(encoder, command, binary("787812800c0000000052656c61792c312300009dee0d0a"));

    }

}
