package com.thingstree.fleetracker.protocol;

import com.thingstree.fleetracker.protocol.TotemProtocolEncoder;
import org.junit.Assert;
import org.junit.Test;
import com.thingstree.fleetracker.ProtocolTest;
import com.thingstree.fleetracker.model.Command;

public class TotemProtocolEncoderTest extends ProtocolTest {

    @Test
    public void testEncode() throws Exception {

        TotemProtocolEncoder encoder = new TotemProtocolEncoder();
        
        Command command = new Command();
        command.setDeviceId(2);
        command.setType(Command.TYPE_ENGINE_STOP);
        command.set(Command.KEY_DEVICE_PASSWORD, "000000");
        
        Assert.assertEquals("*000000,025,C,1#", encoder.encodeCommand(command));

    }

}
