package com.thingstree.fleetracker.protocol;

import com.thingstree.fleetracker.protocol.KhdProtocolEncoder;
import org.junit.Test;
import com.thingstree.fleetracker.ProtocolTest;
import com.thingstree.fleetracker.model.Command;

public class KhdProtocolEncoderTest extends ProtocolTest {

    @Test
    public void testEncode() throws Exception {

        KhdProtocolEncoder encoder = new KhdProtocolEncoder();
        
        Command command = new Command();
        command.setDeviceId(1);
        command.setType(Command.TYPE_ENGINE_STOP);

        verifyCommand(encoder, command, binary("2929390006000000003F0D"));

    }

}
