package com.thingstree.fleetracker.protocol;

import com.thingstree.fleetracker.protocol.HuabaoProtocolEncoder;
import org.junit.Ignore;
import org.junit.Test;
import com.thingstree.fleetracker.ProtocolTest;
import com.thingstree.fleetracker.model.Command;

public class HuabaoProtocolEncoderTest extends ProtocolTest {

    @Ignore
    @Test
    public void testEncode() throws Exception {

        HuabaoProtocolEncoder encoder = new HuabaoProtocolEncoder();
        
        Command command = new Command();
        command.setDeviceId(1);
        command.setType(Command.TYPE_ENGINE_STOP);

        verifyCommand(encoder, command, binary("7EA0060007001403305278017701150424154610AD7E"));

    }

}
