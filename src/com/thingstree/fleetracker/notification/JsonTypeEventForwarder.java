package com.thingstree.fleetracker.notification;

import com.thingstree.fleetracker.model.Event;
import com.thingstree.fleetracker.model.Position;

import com.ning.http.client.AsyncHttpClient.BoundRequestBuilder;

public class JsonTypeEventForwarder extends EventForwarder {

    @Override
    protected String getContentType() {
        return "application/json; charset=utf-8";
    }

    @Override
    protected void setContent(Event event, Position position, BoundRequestBuilder requestBuilder) {
        requestBuilder.setBody(prepareJsonPayload(event, position));
    }

}
