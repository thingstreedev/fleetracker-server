/*
 * Copyright 2017 @thingstree.in
 *
 */
package com.thingstree.fleetracker;

import java.util.Map;

import com.thingstree.fleetracker.model.Event;
import com.thingstree.fleetracker.model.Position;

public abstract class BaseEventHandler extends BaseDataHandler {

    @Override
    protected Position handlePosition(Position position) {

        Map<Event, Position> events = analyzePosition(position);
        if (events != null && Context.getNotificationManager() != null) {
            Context.getNotificationManager().updateEvents(events);
        }
        return position;
    }

    protected abstract Map<Event, Position> analyzePosition(Position position);

}
